import { Injectable, Logger, HttpException, HttpStatus } from '@nestjs/common';
import { MinioService } from 'nestjs-minio-client';
import * as crypto from 'crypto';
import { BufferedFile } from '../models/file.model';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class MinioClientService {


    private readonly logger: Logger;
    private readonly bucketName = this.configService.get('MINIO_BUCKET_NAME');
    private readonly minioEndpoint = this.configService.get('MINIO_ENDPOINT');
    private readonly minioPort = this.configService.get('MINIO_PORT');

    constructor(
        private readonly minio: MinioService,
        private readonly configService: ConfigService,
    ) {
        this.logger = new Logger('MinioService');
    }

    public get client() {
        return this.minio.client;
    }

    public async upload(
        file: BufferedFile,
        bucketName: string = this.bucketName,
    ) {
        if (!(file.mimetype.includes('json'))) {
            throw new HttpException(
                'File type not supported',
                HttpStatus.BAD_REQUEST,
            );
        }
        const timestamp = Date.now().toString();
        const hashedFileName = crypto
            .createHash('md5')
            .update(timestamp)
            .digest('hex');
        const extension = '.json'
        const metaData = {
            'Content-Type': file.mimetype,
        };

        // We need to append the extension at the end otherwise Minio will save it as a generic file
        const fileName = hashedFileName + extension;

        this.client.putObject(
            bucketName,
            fileName,
            file.buffer,
            metaData,
            function (err, res) {
                if (err) {
                    Logger.log(err);
                    throw new HttpException(
                        'Error uploading file',
                        HttpStatus.BAD_REQUEST,
                    );
                }
            },
        );

        return {
            url: `${this.minioEndpoint}:${this.minioPort}/${this.bucketName}/${fileName}`,
        };
    }

    async delete(objetName: string, bucketName: string = this.bucketName) {
        this.client.removeObject(bucketName, objetName, function (err, res) {
            if (err)
                throw new HttpException(
                    'An error occured when deleting!',
                    HttpStatus.BAD_REQUEST,
                );
        });
    }
}